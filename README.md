# Migrate Object Storage Tool

S3互換API対応ストレージ間でのデータ移行用ツール

## 動作環境
* 下記環境で動作確認しています。
    - ruby: 2.1.0 
    - AWS SDK for Ruby(v1)

## インストール
* git clone
```
# git clone https://mz_ken@bitbucket.org/mz_ken/migrate_object_storage_tool.git
```
* mkdir
```
# cd migrate_object_storage_tool
# bundle install --path vendor/bundle
# mkdir tmp log
```

## 設定ファイル
* config.ymlがあるので、それぞれ`endpoint:` `bucketname:` `access_key:` `secret_key:` を設定してください。
    - src: 移行元情報 
    - dst: 移行先情報

## 実行
* 実行コマンド
```
# bundle exec ruby copy_cloudian_to_cd10k.rb
```
    - 移行対象のオブジェクトリスト(listfile)が、並列処理ごとに`./tmp`に出力されます。


## 再開
* 再開コマンド
```
# bundle exec ruby continue_cloudian_to_cd10k_from_file.rb <listfile>
```
    - 指定されたlistfileの先頭から移行を再開します。
    - 実施済みのオブジェクトを消すなど、適宜listfileを編集してから使用してください。

## 補足
* ACLは以下のものだけ対応しています。(ニフクラコンパネのエクスプローラから設定できるものだけ)
    - :public_read
    - :authenticated_read
    - :private
        - 処理が重いのでACL対応は現在無効化してあります。
