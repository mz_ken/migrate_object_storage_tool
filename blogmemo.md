# クラウドストレージ（旧）→オブジェクトストレージへデータ移行する
* 頑張りポイント：なるべくローカルディスクを利用しない<br>
  ※ディスクIOの遅延も気になるし、移行サイズが大きくなるほどディスク容量が必要になってくるため。

## 方法1. s3fs及びaws cli(syncコマンド)を利用する方法
* データの流れ
クラウドストレージ（旧）→ s3fs →　aws cli(syncコマンド) → オブジェクトストレージ

* メリット
    - 導入が簡単

* デメリット
    - ACLがすべて解除される

* 導入準備
    - aws cliのインストール
http://blog.cloud.nifty.com/use_object_storage_with_s3_tool_setting#title5
    - fuseのインストール
```
# wget https://github.com/libfuse/libfuse/releases/download/fuse-2.9.7/fuse-2.9.7.tar.gz
# tar xvfz fuse-2.9.7.tar.gz 
# cd fuse-2.9.7
# ./configure --prefix=/usr
# make
# make install
```
    - s3fsのインストール
```
# yum groupinstall "Development Tools" 
# wget https://github.com/s3fs-fuse/s3fs-fuse/archive/v1.80.tar.gz
# tar xvfz v1.80.tar.gz
# cd s3fs-fuse-1.80
# ./autogen.sh
# ./configure --prefix=/usr
# make
# make install
# echo "<ACCESS_KEY>:<SECRET_KEY>" > ~/.passwd-s3fs 
```

* 実行例：クラウドストレージ（旧）の `srcbucket` からオブジェクトストレージの `dstBucket` に転送する場合
    - s3fsマウント
```
# mkdir /src_dir
# s3fs -f srcbucket  /src_dir -o sigv2 -o url=https://ncss.nifty.com
```
    - aws cliでsync実行
```
# aws s3 sync --endpoint-url https://jp-east-2.os.cloud.nifty.com . s3://dstBucket
```

## 方法2. aws-sdkを利用したプログラムで移行
* メリット
    - ACLがある程度復元できる<br>
      サンプルは3種類のみ

* デメリット
    - プログラムを作成する必要がある

* 導入方法・実行例など
    - http://121.94.80.48/sci01542/migrate_object_storage_tool 参照


